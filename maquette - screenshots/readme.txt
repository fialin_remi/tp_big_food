
La page de connexion sert aussi de page d'inscription.

Aucun retour visuel, de validation d'inscription ou non,
n'est fourni � l'utilisateur. Seul les champs s'effacent.
Il faut alors les retaper pour se connecter.

Le lien de d�connexion ram�ne vers la page de connexion.

Les liens Cr�er et Modifier am�ne vers la m�me page d'�dition,
avec seul diff�rence: les champs pr�-rempli ou non.
La page d'�dition, une fois valid�, ram�ne vers l'affichage des sp�cialit�s.

L'action du lien Supprimer s'ex�cute sur la m�me page:
c'est � dire enl�ve la specialit� de la base de donn�e et de la page.
