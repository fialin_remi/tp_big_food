<%@ page
	language="java"
	contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Edition de spécialités</title>
	</head>
	<body>
		<h1>Edition</h1>
		
		<form action="<c:url value="/createandupdate"/>" method="POST">
			<fieldset>
				<legend></legend>
				<input type="hidden" name="id" value="${ specialite.id }" />
				<label for="image">URL image : </label>
				<input type="text" id="image" name="image" value="${ specialite.image }" />
				<label for="name">Nom : </label>
				<input type="text" id="name" name="name" value="${ specialite.name }" />
				<label for="description">Description : </label>
				<input type="text" id="description" name="description" value="${ specialite.description }" />
				<label for="price">Prix : </label>
				<input type="number" id="price" name="price" value="${ specialite.price }" />
				 <select name="category">
				 	<c:forEach items="${ specialites_categories }" var="categorie">
				 		<option value="${ categorie.id }" <c:if test="${ categorie.id == specialite.category }">selected</c:if>>${ categorie.name }</option>
				 	</c:forEach>
				</select>
				<input type="submit" value="Editer" name="Editer"/>
			</fieldset>
		</form>
	</body>
</html>