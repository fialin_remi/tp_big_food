<%@ page
	language="java"
	contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Spécialités</title>
	</head>
	<body>
		<h1>Nos Spécialités</h1>
		
		<p><a href="<c:url value='/logout'/>">Se déconnecter</a></p>
		
		<c:if test="${ !empty user && user.admin }">
			<a href="<c:url value='/createandupdate'/>">Créer</a>
		</c:if>
		
		<table>
			<thead>
				<tr>
					<th>Image</th>
					<th>Nom</th>
					<th>Description</th>
					<th>Prix</th>
					<c:if test="${ !empty user && user.admin }">
						<th>Actions</th>
					</c:if>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${ !empty specialites }">
						<c:forEach items="${ specialites }" var="specialite">
							<tr>
								<td><img src="${ specialite.image }" alt="${ specialite.image }" height="128" width="128"></td>
								<td>${ specialite.name }</td>
								<td>${ specialite.description }</td>
								<td>${ specialite.price }€</td>
								<c:if test="${ !empty user && user.admin }">
									<td><a href="<c:url value='/createandupdate?id=${ specialite.id }'/>">Modifier</a> <a href="<c:url value='/delete?id=${ specialite.id }'/>">Supprimer</a></td>
								</c:if>
							</tr>
						</c:forEach>
					</c:when>
					<c:otherwise>
						<tr><td colspan="4">La liste des spécialités est actuellement indisponible.</td></tr>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</body>
</html>