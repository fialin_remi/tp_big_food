<%@ page
	language="java"
	contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Identification</title>
	</head>
	<body>
		<h1>Connexion</h1>
		
		<form action="<c:url value="/host"/>" method="POST">
			<fieldset>
				<legend></legend>
				<label for="login">login : </label>
				<input type="text" id="login" name="login" />
				<label for="password">password : </label>
				<input type="password" id="password" name="password"/>
				<input type="submit" value="S'inscrire" name="identificationChoice"/>
				<input type="submit" value="Se connecter" name="identificationChoice"/>
			</fieldset>
		</form>
	</body>
</html>