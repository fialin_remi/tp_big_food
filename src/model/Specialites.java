package model;

import java.io.Serializable;
import java.util.List;

public class Specialites implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	
	private long id;
	private String image;
	private String name;
	private String description;
	private float price;
	private int category;
	
	
	public Specialites(long id, String image, String name, String description, float price, int category)
	{
		this.id = id;
		this.image = image;
		this.name = name;
		this.description = description;
		this.price = price;
		this.category = category;
	}
	
	
	public long getId() { return id; }
	public String getImage() { return image; }
	public String getName() { return name; }
	public String getDescription() { return description; }
	public float getPrice() { return price; }
	public int getCategory() { return category; }
	
	
	public void setId(long id) { this.id = id; }
	public void setImage(String image) { this.image = image; }
	public void setName(String name) { this.name = name; }
	public void setDescription(String description) { this.description = description; }
	public void setPrice(float price) { this.price = price; }
	public void setCategory(int category) { this.category = category; }
	
	
	public static void create(long id, String image, String name, String description, float price, int category)
	{
		dao.Specialites.create(new Specialites(id, image, name, description, price, category));
	}
	
	public static List<Specialites> readAll()
	{
		return dao.Specialites.readAll();
	}
	
	public static Specialites read(long id)
	{
		return dao.Specialites.read(id);
	}
	
	public static void update(long id, String image, String name, String description, float price, int category)
	{
		dao.Specialites.update(new Specialites(id, image, name, description, price, category));
	}
	
	public static void delete(long id)
	{
		dao.Specialites.delete(id);
	}
}
