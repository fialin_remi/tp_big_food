package model;

import java.io.Serializable;

public class Users implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	
	private long id;
	private String login;
	private String password;
	private boolean admin;
	
	
	public Users(long id, String login, String password, boolean admin)
	{
		this.id = id;
		this.login = login;
		this.password = password;
		this.admin = admin;
	}
	
	
	public long getId() { return id; }
	public String getLogin() { return login; }
	public String getPassword() { return password; }
	public Boolean getAdmin() { return admin; }
	
	
	public void setId(long id) { this.id = id; }
	public void setLogin(String login) { this.login = login; }
	public void setPassword(String password) { this.password = password; }
	public void setAdmin(Boolean admin) { this.admin = admin; }
	
	public static void create(String login, String password)
	{
		dao.Users.create(new Users(0, login, password, false));
	}
	
	public static Users read(String login, String password)
	{
		return dao.Users.read(login, password);
	}
}
