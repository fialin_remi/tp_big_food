package model;

import java.io.Serializable;
import java.util.List;

public class JoinSpecialitesCategories implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	
	private long id;
	private String image;
	private String name;
	private String description;
	private float price;
	private int categoryNumber;
	private String categoryName;
	
	
	public JoinSpecialitesCategories(long id, String image, String name, String description, float price,
		int categoryNumber, String categoryName)
	{
		this.id = id;
		this.image = image;
		this.name = name;
		this.description = description;
		this.price = price;
		this.categoryNumber = categoryNumber;
		this.categoryName = categoryName;
	}
	
	
	public long getId() { return id; }
	public String getImage() { return image; }
	public String getName() { return name; }
	public String getDescription() { return description; }
	public float getPrice() { return price; }
	public int getCategoryNunber() { return categoryNumber; }
	public String getCategoryName() { return categoryName; }
	
	
	public void setId(long id) { this.id = id; }
	public void setImage(String image) { this.image = image; }
	public void setName(String name) { this.name = name; }
	public void setDescription(String description) { this.description = description; }
	public void setPrice(float price) { this.price = price; }
	public void setCategoryNumber(int categoryNumber) { this.categoryNumber = categoryNumber; }
	public void setCategoryName(String categoryName) { this.categoryName = categoryName; }
	
	
	public static List<JoinSpecialitesCategories> readAll()
	{
		return dao.JoinSpecialitesCategories.readAll();
	}
}
