package model;

import java.io.Serializable;
import java.util.List;

public class SpecialitesCategories implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	
	private int id;
	private String name;
	
	
	public SpecialitesCategories(int id, String name)
	{
		this.id = id;
		this.name = name;
	}
	
	
	public int getId() { return id; }
	public String getName() { return name; }
	
	public void setId(int id) { this.id = id; }
	public void setName(String name) { this.name = name; }
	
	public static List<SpecialitesCategories> readAll()
	{
		return dao.SpecialitesCategories.readAll();
	}
}
