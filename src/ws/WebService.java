package ws;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



public class WebService extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	
	// � travers une �tiquette nous retenons une classe et une fonction
	private static Map<String, String[]> tabFunctions = new HashMap<String, String[]>();
	
	private WebService() { }
	
	/**
	 * Rempli la Map des fonctions.
	 */
	private static void fillTabFunctions()
	{
		tabFunctions.put("readUser", new String[] {"bl.BL_Users", "read"});
		tabFunctions.put("createUser", new String[] {"bl.BL_Users", "create"});
		
		tabFunctions.put("readAllJoinSpecialitesCategories", new String[] {"bl.BL_JoinSpecialitesCategories", "readAll"});
		
		tabFunctions.put("readAllSpecialitesCategories", new String[] {"bl.BL_SpecialitesCategories", "readAll"});
		
		tabFunctions.put("createSpecialites", new String[] {"bl.BL_Specialites", "create"});
		tabFunctions.put("readSpecialites", new String[] {"bl.BL_Specialites", "read"});
		tabFunctions.put("updateSpecialites", new String[] {"bl.BL_Specialites", "update"});
		tabFunctions.put("deleteSpecialites", new String[] {"bl.BL_Specialites", "delete"});
	}
	
	/**
	 * G�re les demandes utilisateur.
	 * @param request		requ�te utilisateur
	 * @param response		r�ponse � l'utilisateur
	 * @param requestName	nom de la demande/de la requ�te
	 * @return	l'objet r�ponse � la demande
	 * @throws ServletException
	 * @throws IOException
	 */
	public static Object manager(HttpServletRequest request, HttpServletResponse response, String requestName) throws ServletException, IOException
	{
		if (tabFunctions.isEmpty()) { fillTabFunctions(); }
		
		try
		{
			return Class.forName(tabFunctions.get(requestName)[0]).getMethod(
					tabFunctions.get(requestName)[1], new Class[] {HttpServletRequest.class, HttpServletResponse.class}
					).invoke(Class.forName(tabFunctions.get(requestName)[0]).newInstance(), request, response);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			return null;
		}
	}
}
