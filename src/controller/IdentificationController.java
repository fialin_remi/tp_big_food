package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Users;
import ws.WebService;

public class IdentificationController extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	
	public static void identificationControl(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String login = request.getParameter("login");
		String password = request.getParameter("password");
		
		// si nous avons réellement le login et le mot de passe
		if (login != null && password != null
			&& !login.isEmpty() && !password.isEmpty())
		{
			// si c'est une connexion
			if (request.getParameter("identificationChoice").equals("Se connecter"))
			{
				// nous cherchons les informations de cet utilisateur
				Users user = (Users)WebService.manager(request, response, "readUser");
				
				// si cet utilisateur est enregistré en base
				if (user != null)
				{
					// conservation des informations utilisateur
					request.getSession().setAttribute("user", user);
				}
			}
			// sinon c'est une inscription
			else // "S'inscrire"
			{
				// si cet utilisateur n'est pas enregistré en base
				if (WebService.manager(request, response, "readUser") == null)
				{
					// nous l'enregistrons dans la base
					WebService.manager(request, response, "createUser");
				}
			}
		}
	}
}
