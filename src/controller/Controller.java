package controller;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ws.WebService;


public class Controller extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		// si nous n'avons pas d'informations utilisateur
		if (request.getSession().getAttribute("user") == null)
		{
			// fait appel au controleur d'identités
			IdentificationController.identificationControl(request, response);
			
			// si nous n'avons pas d'informations utilisateur
			if (request.getSession().getAttribute("user") == null)
			{
				// demande des informations par identification
				this.getServletContext().getNamedDispatcher("IDENTIFICATION").forward(request, response);
			}
			// si nous avons des informations utilisateur
			else
			{
				// récupération des spécialités dans la base
				request.setAttribute("specialites", WebService.manager(request, response, "readAllJoinSpecialitesCategories"));
				
				// direction la page des spécialités
				
				//request.getRequestDispatcher("/WEB-INF/specialites.jsp").forward(request, response);
				//this.getServletContext().getRequestDispatcher("/WEB-INF/view/specialites.jsp").forward(request, response);
				this.getServletContext().getNamedDispatcher("SPECIALITES").forward(request, response);
			}
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

}
