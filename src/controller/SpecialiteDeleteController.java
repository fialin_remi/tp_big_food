package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ws.WebService;


public class SpecialiteDeleteController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// suppression de la spécialité
		WebService.manager(request, response, "deleteSpecialites");
		
		// récupération des spécialités dans la base
		request.setAttribute("specialites", WebService.manager(request, response, "readAllJoinSpecialitesCategories"));
		
		// retour vers la page des specialites
		this.getServletContext().getNamedDispatcher("SPECIALITES").forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}
}
