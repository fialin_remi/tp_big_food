package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ws.WebService;


public class SpecialiteCreateUpdateController extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// demande de création
		if (request.getParameter("id") == null || request.getParameter("id").isEmpty())
		{
			if (request.getParameter("id") == null)
			{
				// récupération de toutes les catégories de spécialités
				request.setAttribute("specialites_categories", WebService.manager(request, response, "readAllSpecialitesCategories"));
				
				// direction la page de création/modification
				this.getServletContext().getNamedDispatcher("CREER_MODIFIER_SPECIALITES_JSP").forward(request, response);
			}
			else
			{
				// création en base
				WebService.manager(request, response, "createSpecialites");
				
				// récupération des spécialités dans la base
				request.setAttribute("specialites", WebService.manager(request, response, "readAllJoinSpecialitesCategories"));
				
				// direction la page des specialites
				this.getServletContext().getNamedDispatcher("SPECIALITES").forward(request, response);
			}
		}
		// demande d'une modification
		else // request.getParameter("id") != null
		{
			// si nous allons modifier
			if (request.getParameter("Editer") == null)
			{
				// récupération des informations de la spécialités
				request.setAttribute("specialite", WebService.manager(request, response, "readSpecialites"));
				
				// récupération de toutes les catégories de spécialités
				request.setAttribute("specialites_categories", WebService.manager(request, response, "readAllSpecialitesCategories"));
				
				// direction la page de création/modification
				this.getServletContext().getNamedDispatcher("CREER_MODIFIER_SPECIALITES_JSP").forward(request, response);
			}
			// sinon nous avons modifier
			else
			{
				// modification en base
				WebService.manager(request, response, "updateSpecialites");
				
				// récupération des spécialités dans la base
				request.setAttribute("specialites", WebService.manager(request, response, "readAllJoinSpecialitesCategories"));
				
				// direction la page des specialites
				this.getServletContext().getNamedDispatcher("SPECIALITES").forward(request, response);
			}
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}
}
