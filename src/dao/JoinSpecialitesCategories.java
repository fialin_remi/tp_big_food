package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public final class JoinSpecialitesCategories
{
	private JoinSpecialitesCategories() { }
	
	
	public static List<model.JoinSpecialitesCategories> readAll()
	{
		Connection cnx = null;
		
		List<model.JoinSpecialitesCategories> joinSpecialitesCategories = new ArrayList<>();
		
		try
		{
			cnx = Common.getConnection();
			
			ResultSet result = cnx.createStatement().executeQuery(
					"SELECT * FROM specialites s " + 
					"INNER JOIN specialites_categories sc ON s.category = sc.id");
			
			while (result.next())
			{
				joinSpecialitesCategories.add(
					new model.JoinSpecialitesCategories(
						result.getLong("s.id"),
						result.getString("s.image"),
						result.getString("s.name"),
						result.getString("s.description"),
						result.getFloat("s.price"),
						result.getInt("sc.id"),
						result.getString("sc.name")
					)
				);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			Common.closeConnection(cnx);
		}
		
		return joinSpecialitesCategories;
	}
}
