package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public final class Users
{
	private Users() { }
	
	public static void create(model.Users user)
	{
		Connection cnx = null;
		
		try
		{
			cnx = Common.getConnection();
			
			PreparedStatement ps = cnx.prepareStatement(
				"INSERT INTO users (login, password, admin) VALUES (?, ?, 0)"
			);
			
			ps.setString(1, user.getLogin());
			ps.setString(2, user.getPassword());
			
			ps.executeUpdate();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			Common.closeConnection(cnx);
		}
	}
	
	public static model.Users read(String login, String password)
	{
		Connection cnx = null;
		model.Users user = null;
		
		try
		{
			cnx = Common.getConnection();
			
			ResultSet result = cnx.createStatement().executeQuery(
				"SELECT * FROM users"
				+ " WHERE login = '" + login + "'"
				+ " AND password = '" + password + "'"
			);
			
			if (result.next())
			{
				user = new model.Users(
					result.getLong("id"),
					result.getString("login"),
					result.getString("password"),
					result.getBoolean("admin")
				);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			Common.closeConnection(cnx);
		}
		
		return user;
	}
}
