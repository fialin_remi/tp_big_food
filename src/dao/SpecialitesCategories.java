package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public final class SpecialitesCategories
{
	private SpecialitesCategories() { }
	
	public static List<model.SpecialitesCategories> readAll()
	{
		Connection cnx = null;
		
		List<model.SpecialitesCategories> specialitesCategories = new ArrayList<>();
		
		try
		{
			cnx = Common.getConnection();
			
			ResultSet result = cnx.createStatement().executeQuery("SELECT * from specialites_categories");
			
			while (result.next())
			{
				specialitesCategories.add(
					new model.SpecialitesCategories(
						result.getInt("id"),
						result.getString("name")
					)
				);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			Common.closeConnection(cnx);
		}
		
		return specialitesCategories;
	}
	
	public static model.SpecialitesCategories read(int id)
	{
		Connection cnx = null;
		
		model.SpecialitesCategories specialiteCategory = null;
		
		try
		{
			cnx = Common.getConnection();
			
			ResultSet result = cnx.createStatement().executeQuery("SELECT * from specialites_categories WHERE id = " + id);
			
			if (result.next())
			{
				specialiteCategory = new model.SpecialitesCategories(
					result.getInt("id"),
					result.getString("name")
				);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			Common.closeConnection(cnx);
		}
		
		return specialiteCategory;
	}
	
	public static model.SpecialitesCategories read(String name)
	{
		Connection cnx = null;
		
		model.SpecialitesCategories specialiteCategory = null;
		
		try
		{
			cnx = Common.getConnection();
			
			ResultSet result = cnx.createStatement().executeQuery("SELECT * from specialites_categories WHERE name = '" + name + "'");
			
			if (result.next())
			{
				specialiteCategory = new model.SpecialitesCategories(
					result.getInt("id"),
					result.getString("name")
				);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			Common.closeConnection(cnx);
		}
		
		return specialiteCategory;
	}
}
