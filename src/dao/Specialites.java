package dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;


public final class Specialites
{
	private Specialites() { }
	
	public static void create(model.Specialites specialite)
	{
		Connection cnx = null;
		
		try
		{
			cnx = Common.getConnection();
			
			PreparedStatement ps = cnx.prepareStatement(
				"INSERT INTO specialites (image, name, description, price, category) VALUES (?, ?, ?, ?, ?)"
			);
			
			ps.setString(1, specialite.getImage());
			ps.setString(2, specialite.getName());
			ps.setString(3, specialite.getDescription());
			ps.setFloat(4, specialite.getPrice());
			ps.setInt(5, specialite.getCategory());
			
			ps.executeUpdate();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			Common.closeConnection(cnx);
		}
	}
	
	
	public static List<model.Specialites> readAll()
	{
		Connection cnx = null;
		
		List<model.Specialites> specialites = new ArrayList<>();
		
		try
		{
			cnx = Common.getConnection();
			
			ResultSet result = cnx.createStatement().executeQuery("SELECT * FROM specialites");
			
			while (result.next())
			{
				specialites.add(
					new model.Specialites(
						result.getLong("id"),
						result.getString("image"),
						result.getString("name"),
						result.getString("description"),
						result.getFloat("price"),
						result.getInt("category")
					)
				);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			Common.closeConnection(cnx);
		}
		
		return specialites;
	}
	
	public static model.Specialites read(long id)
	{
		Connection cnx = null;
		
		model.Specialites specialite = null;
		
		try
		{
			cnx = Common.getConnection();
			
			ResultSet result = cnx.createStatement().executeQuery("SELECT * FROM specialites WHERE id = " + id);
			
			if (result.next())
			{
				specialite = new model.Specialites(
					result.getLong("id"),
					result.getString("image"),
					result.getString("name"),
					result.getString("description"),
					result.getFloat("price"),
					result.getInt("category")
				);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			Common.closeConnection(cnx);
		}
		
		return specialite;
	}
	
	
	public static void update(model.Specialites specialite)
	{
		Connection cnx = null;
		
		try
		{
			cnx = Common.getConnection();
			
			PreparedStatement ps = cnx.prepareStatement(
				"UPDATE specialites SET image = ?, name = ?, description = ?, price = ?, category = ? WHERE id = ?");
			
			ps.setString(1, specialite.getImage());
			ps.setString(2, specialite.getName());
			ps.setString(3, specialite.getDescription());
			ps.setFloat(4, specialite.getPrice());
			ps.setInt(5, specialite.getCategory());
			ps.setLong(6, specialite.getId());
			
			ps.executeUpdate();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			Common.closeConnection(cnx);
		}
	}
	
	
	public static void delete(long id)
	{
		Connection cnx = null;
		
		try
		{
			cnx = Common.getConnection();
			
			cnx.createStatement().execute("DELETE FROM specialites WHERE id = " + id);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			Common.closeConnection(cnx);
		}
	}
}
