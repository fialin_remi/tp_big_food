package bl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.JoinSpecialitesCategories;

public class BL_JoinSpecialitesCategories
{
	public static List<JoinSpecialitesCategories> readAll(HttpServletRequest request, HttpServletResponse response)
	{
		return model.JoinSpecialitesCategories.readAll();
	}
}
