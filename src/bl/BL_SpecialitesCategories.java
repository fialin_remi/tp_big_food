package bl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.SpecialitesCategories;


public class BL_SpecialitesCategories
{
	public static List<SpecialitesCategories> readAll(HttpServletRequest request, HttpServletResponse response)
	{
		return SpecialitesCategories.readAll();
	}
}
