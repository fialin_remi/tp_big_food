package bl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Users;

public class BL_Users
{
	public static int create(HttpServletRequest request, HttpServletResponse response)
	{
		Users.create(request.getParameter("login"), request.getParameter("password"));
		
		return 1;
	}
	
	public static Users read(HttpServletRequest request, HttpServletResponse response)
	{
		return Users.read(request.getParameter("login"), request.getParameter("password"));
	}
}
