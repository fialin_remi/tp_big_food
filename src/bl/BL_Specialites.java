package bl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Specialites;


public class BL_Specialites
{
	public static int create(HttpServletRequest request, HttpServletResponse response)
	{
		model.Specialites.create(
			// récupération de toutes les modifications
			request.getParameter("id").isEmpty() ? 0 : Long.parseLong(request.getParameter("id")),
			request.getParameter("image"),
			request.getParameter("name"),
			request.getParameter("description"),
			request.getParameter("price").isEmpty() ? 0 : Float.parseFloat(request.getParameter("price")),
			Integer.parseInt(request.getParameter("category"))
		);
		
		return 1;
	}
	
	public static List<Specialites> readAll(HttpServletRequest request, HttpServletResponse response)
	{
		return model.Specialites.readAll();
	}
	
	public static Specialites read(HttpServletRequest request, HttpServletResponse response)
	{
		return model.Specialites.read(Long.parseLong(request.getParameter("id")));
	}
	
	public static int update(HttpServletRequest request, HttpServletResponse response)
	{
		model.Specialites.update(
				// récupération de toutes les modifications
				Long.parseLong(request.getParameter("id")),
				request.getParameter("image"),
				request.getParameter("name"),
				request.getParameter("description"),
				request.getParameter("price").isEmpty() ? 0 : Float.parseFloat(request.getParameter("price")),
				Integer.parseInt(request.getParameter("category"))
		);
		
		return 1;
	}
	
	public static int delete(HttpServletRequest request, HttpServletResponse response)
	{
		model.Specialites.delete(Long.parseLong(request.getParameter("id")));
		
		return 1;
	}
}
